<?php
/*
 Template Name: Photo Gallery
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

		<section id="galleries-content">
		    <div class="wrap cf">
				<div class="responsive-content">
					<h1>Gallery</h1>
					<div class="embed-container">
						<div class="gallery">
							<?php include (TEMPLATEPATH . '/includes/photo-gallery.php' ); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php get_footer(); ?>
