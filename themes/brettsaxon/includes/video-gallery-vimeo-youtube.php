
<ul class="small-block-grid-1 medium-block-grid-3">
<?php
if ( have_rows( 'academy_video_box' ) ) :
  while ( have_rows( 'academy_video_box' ) ) : the_row();
  $type = get_sub_field( 'academy_box_video_toggle' );
  $video_id = get_sub_field('academy_box_video_id');
 
if( $type === 'youtube' ): ?>
<li class="video-block">

  <a href="#" data-reveal-id="<?php the_sub_field('video_short_tag'); ?>">
    <div class="video-wrapper">
    <img src="http://img.youtube.com/vi/<?php echo $video_id ?>/hqdefault.jpg" alt="">
    </div>
    <div class="title-wrapper">
      <h5><?php the_sub_field('academy_box_title'); ?></h5>
    </div>
    
  </a>          
</li>

<div id="<?php the_sub_field('video_short_tag'); ?>" class="reveal-modal large" data-reveal aria-labelledby="videoModalTitle" aria-hidden="true" role="dialog">
  <div class="flex-video widescreen youtube">        
      <iframe width="100%" height="320" src="//www.youtube-nocookie.com/embed/<?php echo $video_id ?>?rel=0" frameborder="0" allowfullscreen></iframe>
    </div>

  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>      
<!-- videos open in lightbox -->      
 
<?php elseif( $type === 'vimeo' ): ?>
 <li class="video-block">
 
  <a href="#" data-reveal-id="<?php the_sub_field('video_short_tag'); ?>">
    <div class="video-wrapper">
    <?php
      echo '<img src="' . get_vimeo_thumb($video_id, 'thumbnail_large') . '">'
    ?>
      
    </div>
    <div class="title-wrapper">
      <h5><?php the_sub_field('academy_box_title'); ?></h5>
    </div>
     
  </a>
 </li>

<div id="<?php the_sub_field('video_short_tag'); ?>" class="reveal-modal large" data-reveal aria-labelledby="videoModalTitle" aria-hidden="true" role="dialog">
  <div class="flex-video widescreen vimeo">          
      <iframe src="http://player.vimeo.com/video/<?php the_sub_field('academy_box_video_id'); ?>?title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="100%" height="320" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
  </div>

  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>      


<!-- videos open in lightbox --> 
  <?php else : ?>
  <p>type var not working</p>
<?php endif;
  endwhile;
else : ?>
<p>have rows not working</p>

<?php endif; ?>
</ul>