	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(function(){
				var $bg;
				$('#menu-main-menu').slicknav({
					label: '',
					closeOnClick: true,
				    init: function() {
				        $bg = $('.slicknav_menu');
				    },
				    afterOpen: function(){
				        $bg.css({'background': 'black'});
				    },
				    afterClose: function(){
				        $bg.css({'background': 'transparent'});
				    }
				});
			});
		});
	</script>