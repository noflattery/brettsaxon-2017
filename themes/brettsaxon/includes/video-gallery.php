
<?php if( have_rows('single_video_gallery') ): ?>
	<div class="single-video-gallery">
	<?php 

	// loop through galleries
	while( have_rows('single_video_gallery') ): the_row(); ?>
		<div>
			<h3><?php the_sub_field('title'); ?></h3>
			<?php 

			// check for attributes
			if( have_rows('attributes') ): ?>
				<ul>
				<?php 

				// loop through rows (sub repeater)
				while(have_rows('attributes')): the_row();
					$type = get_sub_field('source');
					$id = get_sub_field('video_id');

					?>
					<?php if( $type === 'vimeo' ): 

						$vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
						$small = $vimeo[0]['thumbnail_small'];
						$medium = $vimeo[0]['thumbnail_medium'];
						$large = $vimeo[0]['thumbnail_large'];
						$title = $vimeo[0]['title'];
					?>
						
							<li class="video">
								<a href="//vimeo.com/<?php echo $id; ?>" data-lity>
									<?php if( $id ): ?>
										<img src="<?php echo $large; ?>" title="<?php echo $title; ?>"/>
									<?php endif; ?>
								</a>
							</li>

						<div id="inline" style="background:#fff" class="lity-hide">
							Inline content
						</div>

					<?php elseif( $type === 'youtube' ): 

						$youtube = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id={$video_ID}&key=AIzaSyCvfSf4gw0aP-fjViBmm3QzkVb5xI9xyUM&part=statistics");
						// $title = $youtube[0]['title'];
					?>

						
						<li class="video">
							<a href="//www.youtube.com/watch?v=<?php echo $id; ?>" data-lity>
								<img src="http://img.youtube.com/vi/<?php echo $id ?>/hqdefault.jpg" alt="">
							</a>
						</li>


						<div id="inline" style="background:#fff" class="lity-hide">
							<?php echo $title; ?>
							Inline content
						</div>
					<?php endif; ?>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>	

		<div id="inline" style="background:#fff" class="lity-hide">
		Inline content
		</div>

	<?php endwhile;  ?>
	</div>
<?php endif; ?>


