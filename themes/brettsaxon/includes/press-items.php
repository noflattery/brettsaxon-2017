
<?php if( have_rows('press_items') ): ?>
	<div class="press-items">
	<?php 
		while( have_rows('press_items') ): the_row(); 
			$attachment = get_sub_field('attachment');
			$summary = get_sub_field('item_summary');
		?>
			<div>
				<ul>
							
					<li class="press-item">
						<h3><?php echo $summary; ?></h3>
						<?php if(get_sub_field('attachment')): ?>
							<a href="<?php echo $attachment['url']; ?>" target="_blank">
								DOWNLOAD
							</a>
						<?php endif; ?>
					</li>					
				</ul>
			</div>	
		<?php endwhile;  
	?>
	</div>
<?php endif; ?>


