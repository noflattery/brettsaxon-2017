<?php $merch_header = get_field('merch_header'); ?>
<?php if( have_rows('products') ): ?>
	<?php while( have_rows('products') ): the_row(); ?>
		<h3><?php echo $merch_header; ?></h3>
			<div class="merch-items">
					<div id="merch-items">
						<ul class="flex-merch">
							<?php if( have_rows('product') ): ?>
								<?php while( have_rows('product') ): the_row();
									$image = get_sub_field('image');
									$description = get_sub_field('description');
									$link = get_sub_field('link');
								?>
									<li class="merch-item m-1of2 t-1of4 d-1of4 cf">
											<a href="<?php echo $link; ?>" target="_blank">
												<img src="<?php echo $image['url']; ?>" />
												<p><?php echo $description; ?></p>
											</a>
									</li>
								<?php endwhile;?>
							<?php endif; ?>
						</ul>
					</div>
			</div>
	<?php endwhile; ?>
<?php endif; ?>
