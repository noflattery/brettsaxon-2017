
<?php if( have_rows('photo_gallery') ): ?>
	<div class="photo-gallery">
	<?php 

	// loop through galleries
	while( have_rows('photo_gallery') ): the_row(); ?>
		<div>
			<h3><?php the_sub_field('title'); ?></h3>
			<?php 

			// check for attributes
			if( have_rows('attributes') ): ?>
				<ul>
					<?php 
					// loop through rows (sub repeater)
					while(have_rows('attributes')): the_row();
						// $vimeo = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
						// $small = $vimeo[0]['thumbnail_small'];
						// $medium = $vimeo[0]['thumbnail_medium'];
						// $large = $vimeo[0]['thumbnail_large'];
						// $title = $vimeo[0]['title'];

						$image = get_sub_field('image');
						$caption = get_sub_field('caption')
					?>
							
						<li class="photo">
							<a href="<?php echo $image['url']; ?>" data-lity>
								<img src="<?php echo $image['url']; ?>" />
							</a>
								
						</li>

					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>	
	<?php endwhile;  ?>
	</div>
<?php endif; ?>


