<?php
 
// $clientid = "a90dc114942f9ccd2478ca3eb3948a5c"; // Your API Client ID
// $userid = "4018638"; // ID of the user you are fetching the information for
 
// $soundcloud_url = "http://api.soundcloud.com/users/{$userid}/tracks.json?client_id={$clientid}";
 
// $tracks_json = file_get_contents($soundcloud_url);
// $tracks = json_decode($tracks_json);
 
// foreach ($tracks as $track) {
 
//     echo $track->title . "
// ";
// }

$user_id = get_field('soundcloud_handle');
$music_title = get_field('music_title');

?>

<?php if (get_field('page_title')) : ?>
	<h1><?php echo $music_title; ?></h1> 
<?php endif; ?>
  
<div class="post">

<?php if( have_rows('soundcloud_tracks') ): ?>
	<ul class="songs">
		<?php while( have_rows('soundcloud_tracks') ): the_row(); 

			// vars
			$track_handle = get_sub_field('track_handle');

			?>

			<li>
				<a href="http://soundcloud.com/<?php echo $user_id; ?>/<?php echo $track_handle; ?>" class="sc-player">sampletrack</a>
			</li>
			
			
		<?php endwhile; ?>
	</ul>
<?php endif; ?>


</div>
    
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/soundcloud.player.api.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/sc-player.js"></script>