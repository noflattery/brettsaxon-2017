<?php
/*
 Template Name: Videos
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

		<section id="videos-content">
		    <div class="wrap cf">
				<div class="responsive-content">
					<h1>Videos</h1>
					<div class="embed-container">
						<div class="gallery">
							<?php include (TEMPLATEPATH . '/includes/video-gallery.php' ); ?>
						</div>

						<?php if(get_field('video_channel_link')): ?>
							<a href="<?php the_field('video_channel_link'); ?>" target="_blank" style="display: block;">
						
								<div class="video-links">
									<ul>
										<li class="video-copy">
											<?php if(get_field('video_copy_start')): ?>
												<?php the_field('video_copy_start'); ?>
											<?php endif; ?>
										</li>

										<li class="fa fa-youtube"></li>

										<li class="video-copy">
											<?php if(get_field('video_copy_end')): ?>
												<?php the_field('video_copy_end'); ?>
											<?php endif; ?>
										</li>

									</ul>
								</div>
							</a
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>

<?php get_footer(); ?>
