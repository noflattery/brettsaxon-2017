<?php
/*
 Template Name: Home Page
*/
?>

<?php get_header(); ?>

			<div id="content">
				<?php if( have_rows('home_section') ): ?>
					<?php while( have_rows('home_section') ): the_row();
						$home_background = get_sub_field('home_background');
					?>

						<a id="home"></a>
						<section id="home-content" style="background: url(<?php echo $home_background['url']; ?>);">
							<div class="wrap cf">
								<div class="responsive-content">
								</div>
							</div>
						</section>

					<?php endwhile; ?>
				<?php endif; ?>

				<?php if( have_rows('about_section') ): ?>
					<?php while( have_rows('about_section') ): the_row();

						// vars
						$about_header = get_sub_field('about_header');
						$about_text = get_sub_field('about_text');

						?>

						<a id="about"></a>
						<section id="about-content">
							<div class="about-content">
								<div class="wrap cf">
									<div class="responsive-content">
										<h2><?php echo $about_header; ?></h2>
										<p><?php echo $about_text; ?></p>
									</div>
								</div>
							</div>

						</section>

					<?php endwhile; ?>
				<?php endif; ?>

				<?php
					$music_background = get_field('music_background');
				?>

				<a id="audio"></a>
				<section id="music-content" style="background: url(<?php echo $music_background['url']; ?>);">
					<div class="wrap cf">
						<div class="responsive-content">
							<h2>Audio</h2>
							<div class="m-all d-1of2 t-1of3 album-songs">
								<?php include (TEMPLATEPATH . '/includes/soundcloudapi.php' ); ?>
							</div>

							<div class="m-all d-1of2 t-2of3">
								<div class="music-description">
									<?php
										$music_description = get_field('music_description');
									?>
									<?php echo $music_description; ?>
								</div>
								<div class="buy-stream-links">
									<div class="featured-album">
										<?php
											$album = get_field('featured_album_image');
										?>

										<img src="<?php echo $album['url']; ?>" alt="<?php echo $album['alt'] ?>" />
										<?php if( have_rows('buy_links') ): ?>
											<ul class="buy-links">
												<?php while( have_rows('buy_links') ): the_row();

													// vars
													$link_title = get_sub_field('link_title');
													$link_url = get_sub_field('link_url');

												?>

												<li class="single-buy-link">
													<a href="<?php echo $link_url; ?>" target="_blank">
													</a>
												</li>


												<?php endwhile; ?>
											</ul>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>

				</section>

				<a id="videos"></a>
				<section id="videos-content">
				    <div class="wrap cf">
						<div class="responsive-content">
							<h2>Videos</h2>
							<?php if( get_field('video_id') ): ?>
								<div class="video-content">
									<div class="embed-container">
										<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $videolink = the_field('video_id'); ?>" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</section>
				<?php
					$shows_background = get_field('shows_background');
				?>

				<a id="shows"></a>
				<section id="shows-content" style="background: url(<?php echo $shows_background['url']; ?>);">
					<div class="wrap cf">
						<div class="responsive-content">
							<h2>Shows</h2>
							<?php echo do_shortcode('[bandsintown_events]'); ?>
						</div>
					</div>
				</section>


				<?php
				$user_id = get_field('soundcloud_handle');
				$music_title = get_field('music_title');
				?>

				<?php if (get_field('page_title')) : ?>
					<h1><?php echo $music_title; ?></h1>
				<?php endif; ?>

				<a id="merch"></a>
				<section id="merch-content">
				    <div class="wrap cf">
						<div class="responsive-content">
							<h2>Merch</h2>
							<div class="embed-container">
								<div class="products">
									<?php include (TEMPLATEPATH . '/includes/merch-items.php' ); ?>
								</div>
							</div>
						</div>
					</div>
				</section>


				<a id="pics"></a>
				<section id="galleries-content">
				    <div class="wrap cf">
						<div class="responsive-content">
							<h2>Pics</h2>
							<div class="embed-container">
								<div class="gallery">
									<?php echo do_shortcode('[instagram-feed]'); ?>
								</div>
							</div>
						</div>
					</div>
				</section>

				<a id="contact"></a>
				<section id="contact-content">

				<?php
					$contact_header = get_field('contact_header');
				?>

					<?php include (TEMPLATEPATH . '/includes/contact.php' ); ?>

				</section>
			</div>

<script type="text/javascript">
	// (function($) {
	// 	var vHeight = $(window).height(),
 //   			vWidth = $(window).width(),
 //    		cover = $('section');

	// 		cover.css({"height":vHeight,"width":vWidth});
	// })(jQuery);
</script>

<?php get_footer(); ?>
