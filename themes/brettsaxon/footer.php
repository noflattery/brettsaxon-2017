			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

				</div>

			</footer>

		</div>
		<!-- cant get this to register in bones ... -->
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/jquery.slicknav.min.js"></script>		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/smooth-scroll.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/library/js/libs/lity.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

		<?php include (TEMPLATEPATH . '/includes/slicknav.php' ); ?>

		<script type="text/javascript">
			
			jQuery(document).ready(function($){
				$('#menu-main-menu li a').each(function(){
					$(this).attr('data-scroll', '');
				});

				smoothScroll.init({
					selector: '[data-scroll]', // Selector for links (must be a valid CSS selector)
					selectorHeader: '[data-scroll-header]', // Selector for fixed headers (must be a valid CSS selector)
					speed: 500, // Integer. How fast to complete the scroll in milliseconds
					easing: 'easeInOutCubic', // Easing pattern to use
					offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
					updateURL: true, // Boolean. If true, update the URL hash on scroll
					callback: function ( anchor, toggle ) {} // Function to run after scrolling
				});

				// $('#menu-main-menu li:nth-child(3) a, #menu-main-menu li:nth-child(5) a, #menu-main-menu li:nth-child(7) a').removeAttr('data-scroll');
				// $('.page-template-page-videos #menu-main-menu li a').removeAttr('data-scroll');
				// $('.page-template-page-gallery #menu-main-menu li a').removeAttr('data-scroll');
				// $('.page-template-page-press #menu-main-menu li a').removeAttr('data-scroll');

				// $('#menu-main-menu ul li:first-child a').attr('name').val('about');

				$('.gigpress-table tbody').hide();
				$('.gigpress-table tbody:first-child, .gigpress-table tbody:nth-child(2), .gigpress-table tbody:nth-child(3)').show();
				$('.down-arrow').on('click', function(){
					$(this).hide();
					$('.gigpress-table tbody').fadeIn();
				});

				$('.gigpress-tickets-link').attr('target', '_blank'); 

			});
			
		</script>

	</body>

</html> <!-- end of site. what a ride! -->
